package utils

import (
	"api/constants"
	"crypto/md5"
	"encoding/hex"
	"sort"
	"strings"
)

/**
 * @Author: llooper
 * @Date: 2021/7/17 14:39
 */

func JoinStringsInASCII(data map[string]string, sep string, includeEmpty bool, exceptKeys ...string) (string, []string) {
	var list []string
	var keyList []string
	m := make(map[string]int)
	if len(exceptKeys) > 0 {
		for _, except := range exceptKeys {
			m[except] = 1
		}
	}
	for k := range data {
		if _, ok := m[k]; ok {
			continue
		}
		value := data[k]
		if !includeEmpty && value == "" {
			continue
		}
		keyList = append(keyList, k)
	}
	sort.Strings(keyList)
	for _, v := range keyList {
		list = append(list, data[v])
	}
	return strings.Join(list, sep), keyList
}

func Sign(data map[string]string) string {
	paramVal, _ := JoinStringsInASCII(data, "", true, "sign")
	return Md5V(paramVal + constants.SecretKey)
}

func Md5V(str string) string {
	h := md5.New()
	h.Write([]byte(str))
	return hex.EncodeToString(h.Sum(nil))
}
