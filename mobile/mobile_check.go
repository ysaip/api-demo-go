package main

import (
	"api/constants"
	"api/token"
	"api/utils"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

var (
	mobileElem2URI  = "/aip/check/v1/mobile/elem2"
	mobileElem3URI  = "/aip/check/v1/mobile/elem3"
	mobileOnlineURI = "/aip/check/v1/mobile/online"
)

func main() {
	var body []byte
	body, _ = MobileElem2("uuid", "张三", "150124747xx")
	fmt.Println(string(body))

	body, _ = MobileElem3("uuid", "张三", "440804199x060802xx", "150124747xx")
	fmt.Println(string(body))

	body, _ = MobileOnline("uuid", "150124747xx")
	fmt.Println(string(body))
}

/**
手机二要素核验接口
_BizNo：用户自定义业务ID
idName：姓名
phoneNumber：手机号码
*/
func MobileElem2(_BizNo, idName, phoneNumber string) ([]byte, error) {
	url := fmt.Sprintf("%s%s?access_token=%s", constants.Host, mobileElem2URI, token.Token.GetToken())
	var m = make(map[string]string)
	m["appId"] = constants.AppId
	m["idName"] = idName
	m["phoneNumber"] = phoneNumber
	m["_BizNo"] = _BizNo
	m["sign"] = utils.Sign(m)
	mJson, _ := json.Marshal(m)
	req, _ := http.NewRequest("POST", url, bytes.NewReader(mJson))
	req.Header.Add("Content-Type", "application/json")
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Errorf("MobileElem2 err: %+v", err)
		return nil, err
	}
	defer res.Body.Close()
	return ioutil.ReadAll(res.Body)
}

/**
手机三要素核验接口
_BizNo：用户自定义业务ID
idName：姓名
idNumber：身份证号码
phoneNumber：手机号码
*/
func MobileElem3(_BizNo, idName, idNumber, phoneNumber string) ([]byte, error) {
	url := fmt.Sprintf("%s%s?access_token=%s", constants.Host, mobileElem3URI, token.Token.GetToken())
	var m = make(map[string]string)
	m["appId"] = constants.AppId
	m["idName"] = idName
	m["idNumber"] = idNumber
	m["phoneNumber"] = phoneNumber
	m["_BizNo"] = _BizNo
	m["sign"] = utils.Sign(m)
	mJson, _ := json.Marshal(m)
	req, _ := http.NewRequest("POST", url, bytes.NewReader(mJson))
	req.Header.Add("Content-Type", "application/json")
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Errorf("MobileElem3 err: %+v", err)
		return nil, err
	}
	defer res.Body.Close()
	return ioutil.ReadAll(res.Body)
}

/**
手机在网时长查询
_BizNo：用户自定义业务ID
phoneNumber：手机号码
*/
func MobileOnline(_BizNo, phoneNumber string) ([]byte, error) {
	url := fmt.Sprintf("%s%s?access_token=%s", constants.Host, mobileOnlineURI, token.Token.GetToken())
	var m = make(map[string]string)
	m["appId"] = constants.AppId
	m["phoneNumber"] = phoneNumber
	m["_BizNo"] = _BizNo
	m["sign"] = utils.Sign(m)
	mJson, _ := json.Marshal(m)
	req, _ := http.NewRequest("POST", url, bytes.NewReader(mJson))
	req.Header.Add("Content-Type", "application/json")
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Errorf("mobileOnline err: %+v", err)
		return nil, err
	}
	defer res.Body.Close()
	return ioutil.ReadAll(res.Body)
}
