package main

import (
	"api/constants"
	"api/token"
	"api/utils"
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

var (
	idElem2URI = "/aip/check/v1/id/elem2"
	idElem3URI = "/aip/check/v1/id/elem3"
)

func main() {
	var body []byte
	body, _ = IdElem2("uuid", "张三", "440804199x060802xx")
	fmt.Println(string(body))
	faceImage, _ := ioutil.ReadFile("D:\\Downloads\\ysaip\\393444208641310723_cdcf2cfab2baf4f7eae49b632986e928.jpg")
	body, _ = IdElem3("uuid", "张三", "440804199x060802xx", base64.StdEncoding.EncodeToString(faceImage))
	fmt.Println(string(body))
}

/**
身份证二要素核验
_BizNo：用户自定义业务ID
cardName：姓名
cardCode：身份证号码
*/
func IdElem2(_BizNo, cardName, cardCode string) ([]byte, error) {
	url := fmt.Sprintf("%s%s?access_token=%s", constants.Host, idElem2URI, token.Token.GetToken())
	var m = make(map[string]string)
	m["appId"] = constants.AppId
	m["cardName"] = cardName
	m["cardCode"] = cardCode
	m["_BizNo"] = _BizNo
	m["sign"] = utils.Sign(m)
	mJson, _ := json.Marshal(m)
	req, _ := http.NewRequest("POST", url, bytes.NewReader(mJson))
	req.Header.Add("Content-Type", "application/json")
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Errorf("idElem2 err: %+v", err)
		return nil, err
	}
	defer res.Body.Close()
	return ioutil.ReadAll(res.Body)
}

/**
身份证三要素核验
_BizNo：用户自定义业务ID
cardName：姓名
cardCode：身份证号码
faceImage：人像BASE64
*/
func IdElem3(_BizNo, cardName, cardCode, faceImage string) ([]byte, error) {
	url := fmt.Sprintf("%s%s?access_token=%s", constants.Host, idElem3URI, token.Token.GetToken())
	var m = make(map[string]string)
	m["appId"] = constants.AppId
	m["cardName"] = cardName
	m["cardCode"] = cardCode
	m["faceImage"] = faceImage
	m["_BizNo"] = _BizNo
	m["sign"] = utils.Sign(m)
	mJson, _ := json.Marshal(m)
	req, _ := http.NewRequest("POST", url, bytes.NewReader(mJson))
	req.Header.Add("Content-Type", "application/json")
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Errorf("idElem3 err: %+v", err)
		return nil, err
	}
	defer res.Body.Close()
	return ioutil.ReadAll(res.Body)
}
