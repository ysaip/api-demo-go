package token

import (
	"api/constants"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"
)

var (
	accessTokenURI = "/aip/oauth/2.0/token"
	threshold      = 24

	Token = &AccessToken{}
)

type AccessToken struct {
	Token string
	Time  time.Time
}

func (t *AccessToken) GetToken() string {
	sub := int(time.Now().Sub(t.Time).Hours())
	if sub >= threshold || t.Token == "" {
		t.refreshToken()
	}
	return t.Token
}

func (t *AccessToken) refreshToken() (string, error) {
	payload := url.Values{}
	payload.Set("grant_type", constants.GrantType)
	payload.Set("client_id", constants.ApiKey)
	payload.Set("client_secret", constants.SecretKey)
	req, _ := http.NewRequest("POST", constants.Host+accessTokenURI, strings.NewReader(payload.Encode()))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Errorf("err:%v", err)
		return "", err
	}
	defer res.Body.Close()
	jsonBytes, err := ioutil.ReadAll(res.Body)
	var m = make(map[string]string)
	if err = json.Unmarshal(jsonBytes, &m); err != nil {
		fmt.Errorf("err:%v", err)
	}
	t.Time = time.Now()
	t.Token = m["access_token"]
	return t.Token, err
}
