package main

import (
	"api/constants"
	"api/token"
	"api/utils"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

var (
	epElem3URI = "/aip/check/v1/ep/elem3"
	epElem4URI = "/aip/check/v1/ep/elem4"
)

func main() {
	var body []byte
	body, _ = EpElem3("uuid", "公司名称", "xxx", "张三")
	fmt.Println(string(body))

	body, _ = EpElem4("uuid", "公司名称", "xxxx", "张三", "20100303", "长期")
	fmt.Println(string(body))
}

/**
企业三要素核验
_BizNo：用户自定义业务ID
companyName：公司
creditCode：统一社会信用代码
legalPerson：法人姓名
*/
func EpElem3(_BizNo, companyName, creditCode, legalPerson string) ([]byte, error) {
	url := fmt.Sprintf("%s%s?access_token=%s", constants.Host, epElem3URI, token.Token.GetToken())
	var m = make(map[string]string)
	m["appId"] = constants.AppId
	m["companyName"] = companyName
	m["creditCode"] = creditCode
	m["legalPerson"] = legalPerson
	m["_BizNo"] = _BizNo
	m["sign"] = utils.Sign(m)
	mJson, _ := json.Marshal(m)
	req, _ := http.NewRequest("POST", url, bytes.NewReader(mJson))
	req.Header.Add("Content-Type", "application/json")
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Errorf("MobileElem2 err: %+v", err)
		return nil, err
	}
	defer res.Body.Close()
	return ioutil.ReadAll(res.Body)
}

/**
企业三要素核验
_BizNo：用户自定义业务ID
companyName: 企业名称
creditCode: 统一社会信用代码
legalPerson: 法人姓名
startPeriod: 营业期限起始日期，请严格按照以下格式传入，如： 20100303，否则可能导致比对有误差
endPeriod: 营业期限截止日期，请严格按照以下格式传入，如： 20300302，否则可能导致比对有误差 如果执照上的截至日期是"长期"，则传入"长期"
*/
func EpElem4(_BizNo, companyName, creditCode, legalPerson, startPeriod, endPeriod string) ([]byte, error) {
	url := fmt.Sprintf("%s%s?access_token=%s", constants.Host, epElem4URI, token.Token.GetToken())
	var m = make(map[string]string)
	m["appId"] = constants.AppId
	m["companyName"] = companyName
	m["creditCode"] = creditCode
	m["legalPerson"] = legalPerson
	m["startPeriod"] = startPeriod
	m["endPeriod"] = endPeriod
	m["_BizNo"] = _BizNo
	m["sign"] = utils.Sign(m)
	mJson, _ := json.Marshal(m)
	req, _ := http.NewRequest("POST", url, bytes.NewReader(mJson))
	req.Header.Add("Content-Type", "application/json")
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Errorf("MobileElem2 err: %+v", err)
		return nil, err
	}
	defer res.Body.Close()
	return ioutil.ReadAll(res.Body)
}
