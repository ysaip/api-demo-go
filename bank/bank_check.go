package main

import (
	"api/constants"
	"api/token"
	"api/utils"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

var (
	bankElem2URI = "/aip/check/v1/bank/elem2"
	bankElem3URI = "/aip/check/v1/bank/elem3"
	bankElem4URI = "/aip/check/v1/bank/elem4"
	bankElem5URI = "/aip/check/v1/bank/elem5"
)

func main() {
	var body []byte
	body, _ = BankElem2("uuid", "张三", "8445555xxx")
	fmt.Println(string(body))

	body, _ = BankElem3("uuid", "张三", "440804xxx", "8445555xxx")
	fmt.Println(string(body))

	body, _ = BankElem4("uuid", "张三", "440804xxx", "8445555xxx", "")
	fmt.Println(string(body))

	body, _ = BankElem5("uuid", "张三", "440804xxx", "8445555xxx", "", "1")
	fmt.Println(string(body))
}

/**
银行卡二要素核验
_BizNo：用户自定义业务ID
idName：姓名
bankCard：银行卡号
*/
func BankElem2(_BizNo, idName, bankCard string) ([]byte, error) {
	url := fmt.Sprintf("%s%s?access_token=%s", constants.Host, bankElem2URI, token.Token.GetToken())
	var m = make(map[string]string)
	m["appId"] = constants.AppId
	m["idName"] = idName
	m["bankCard"] = bankCard
	m["_BizNo"] = _BizNo
	m["sign"] = utils.Sign(m)
	mJson, _ := json.Marshal(m)
	req, _ := http.NewRequest("POST", url, bytes.NewReader(mJson))
	req.Header.Add("Content-Type", "application/json")
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Errorf("idElem2 err: %+v", err)
		return nil, err
	}
	defer res.Body.Close()
	return ioutil.ReadAll(res.Body)
}

/**
银行卡三要素核验
_BizNo：用户自定义业务ID
idName：姓名
idNumber：身份证号
bankCard：银行卡号
*/
func BankElem3(_BizNo, idName, idNumber, bankCard string) ([]byte, error) {
	url := fmt.Sprintf("%s%s?access_token=%s", constants.Host, bankElem3URI, token.Token.GetToken())
	var m = make(map[string]string)
	m["appId"] = constants.AppId
	m["idName"] = idName
	m["idNumber"] = idNumber
	m["bankCard"] = bankCard
	m["_BizNo"] = _BizNo
	m["sign"] = utils.Sign(m)
	mJson, _ := json.Marshal(m)
	req, _ := http.NewRequest("POST", url, bytes.NewReader(mJson))
	req.Header.Add("Content-Type", "application/json")
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Errorf("idElem2 err: %+v", err)
		return nil, err
	}
	defer res.Body.Close()
	return ioutil.ReadAll(res.Body)
}

/**
银行卡四要素核验
_BizNo：用户自定义业务ID
idName：姓名
idNumber：身份证号
bankCard：银行卡号
phoneNumber：手机号码
*/
func BankElem4(_BizNo, idName, idNumber, bankCard, phoneNumber string) ([]byte, error) {
	url := fmt.Sprintf("%s%s?access_token=%s", constants.Host, bankElem4URI, token.Token.GetToken())
	var m = make(map[string]string)
	m["appId"] = constants.AppId
	m["idName"] = idName
	m["idNumber"] = idNumber
	m["bankCard"] = bankCard
	m["phoneNumber"] = phoneNumber
	m["_BizNo"] = _BizNo
	m["sign"] = utils.Sign(m)
	mJson, _ := json.Marshal(m)
	req, _ := http.NewRequest("POST", url, bytes.NewReader(mJson))
	req.Header.Add("Content-Type", "application/json")
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Errorf("idElem2 err: %+v", err)
		return nil, err
	}
	defer res.Body.Close()
	return ioutil.ReadAll(res.Body)
}

/**
银行卡五要素核验
_BizNo：用户自定义业务ID
idName：姓名
idNumber：身份证号
bankCard：银行卡号
phoneNumber：手机号码
bankCardType：卡类型可选取值 1：I类，2：II类 3：III类
*/
func BankElem5(_BizNo, idName, idNumber, bankCard, phoneNumber, bankCardType string) ([]byte, error) {
	url := fmt.Sprintf("%s%s?access_token=%s", constants.Host, bankElem5URI, token.Token.GetToken())
	var m = make(map[string]string)
	m["appId"] = constants.AppId
	m["idName"] = idName
	m["idNumber"] = idNumber
	m["bankCard"] = bankCard
	m["phoneNumber"] = phoneNumber
	m["bankCardType"] = bankCardType
	m["_BizNo"] = _BizNo
	m["sign"] = utils.Sign(m)
	mJson, _ := json.Marshal(m)
	req, _ := http.NewRequest("POST", url, bytes.NewReader(mJson))
	req.Header.Add("Content-Type", "application/json")
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Errorf("idElem2 err: %+v", err)
		return nil, err
	}
	defer res.Body.Close()
	return ioutil.ReadAll(res.Body)
}
